let salir_busk = document.querySelector("#salirBusqueda");
salir_busk.disabled = true;
let titulo = document.querySelector("#encabezado");
let seccionProducto = document.querySelector("#productos");
let descripcion = document.querySelector("#desc");
let urlImagen = document.querySelector("#dmgI");
let boton_uno = document.querySelector("#btn_uno");
let boton_dos = document.querySelector("#btn_dos");
let arrayMisproductos = [];
let producto = {
    descripcion: "",
    imagen: ""
}
let des, img;

if (localStorage.getItem('productos')) {
    arrayMisproductos = JSON.parse(localStorage.getItem('productos'));
} else {
    producto = {
        descripcion: "Parlante Portatil Melon Bluetooth 8 Pulgadas Luces Led​",
        imagen: "https://http2.mlstatic.com/D_NQ_NP_2X_618548-MLA44866133508_022021-F.webp"
    }
    arrayMisproductos.push(producto);

    producto = {
        descripcion: "Microsoft Xbox Series S 512GB Standard color blanco",
        imagen: "https://http2.mlstatic.com/D_NQ_NP_2X_627149-MLA44484230438_012021-F.webp"
    }
    arrayMisproductos.push(producto);
}
seccionProducto.innerHTML = armarTemplate();

function agregar() {
    des = descripcion.value.trim();
    img = urlImagen.value.trim();
    if (des.length === 0 || img.length === 0) return;
    producto = {
        descripcion: des,
        imagen: img
    }

    arrayMisproductos.push(producto);
    console.log(arrayMisproductos);
    seccionProducto.innerHTML = armarTemplate();

}
function armarTemplate() {
    let template = '';
    for (let i = 0; i < arrayMisproductos.length; i++) {
        template += `<article>
        <div class="elim" onclick="eliminarItem(${i})"><img src="elim.png"></div>
        <div class="edit" onclick="editarItem(${i})"><img src="pen-edit.png"></div>           
        <h2 class="descripcion">${arrayMisproductos[i].descripcion}</h2>
                    <img src="${arrayMisproductos[i].imagen}" class="imagen" >
                     </article>`
    }
    return template;
}

function listado() {
    if (arrayMisproductos.length > 0) localStorage.setItem('productos', JSON.stringify(arrayMisproductos));
    location.href = 'sigPag.html';
}

function eliminarItem(numPro) {
    arrayMisproductos.splice(numPro, 1);

    if (arrayMisproductos.length === 0) {
        localStorage.removeItem('productos');
    } else {
        localStorage.setItem('productos', JSON.stringify(arrayMisproductos));
    }
    // location.href = 'index.html';
    seccionProducto.innerHTML = armarTemplate();
}
function editarItem(numPro) {
    titulo.innerHTML = "Edición de Productos";

    producto = arrayMisproductos[numPro];
    descripcion.value = producto.descripcion;
    urlImagen.value = producto.imagen;

    boton_uno.value = "Modificar";
    boton_uno.classList.add("color_blue");
    boton_dos.value = "Cancelar";
    boton_dos.classList.add("color_red");

    seccionProducto.innerHTML = armarTemplateSbtns();

    boton_uno.setAttribute("onclick", `modificarItem(${numPro})`);
    boton_dos.setAttribute("onclick", "delet_Cancelar()");
}

function armarTemplateSbtns() {
    let template = '';
    for (let i = 0; i < arrayMisproductos.length; i++) {
        template += `<article>
            <h2 class="descripcion">${arrayMisproductos[i].descripcion}</h2>
            <img src="${arrayMisproductos[i].imagen}" class="imagen" >
                     </article>`
    }
    return template;
}
function modificarItem(numPro) {
    des = descripcion.value.trim();
    img = urlImagen.value.trim();

    if (des.length === 0 || img.length === 0) return;
    producto = {
        descripcion: des,
        imagen: img
    }
    arrayMisproductos[numPro] = producto;
    delet_Cancelar();
    localStorage.setItem('productos', JSON.stringify(arrayMisproductos));

}


function delet_Cancelar() {
    descripcion.value = "";
    urlImagen.value = "";
    titulo.innerHTML = "Nuevo Producto";
    boton_uno.value = "Agregar";
    boton_dos.value = "Listado";
    boton_uno.classList.remove("color_blue");
    boton_dos.classList.remove("color_red");
    boton_uno.setAttribute("onclick", "agregar()");
    boton_dos.setAttribute("onclick", "listado()");

    seccionProducto.innerHTML = armarTemplate();
}
function buscamos() {
    document.querySelector("#agregar").classList.add("disable");
    salir_busk.disabled = false;
    let buscar = document.querySelector("#buscar").value;
    if (buscar.trim().length === 0) {
        seccionProducto.innerHTML = armarTemplateSbtns();
    } else {
        let array_busqueda = [], k = -1;
        for (let i = 0; i < arrayMisproductos.lengt; i++) {
            producto = arrayMisproductos[i];
            if (producto.descripcion.toLowerCase().startsWith(lupa.toLowerCase())) {
                k++;
                array_busqueda[k] = i;
            }
            console.log("Array_busqueda: ", array_busqueda);
            seccionProducto.innerHTML = armaTemplateBusqueda(array_busqueda);

        }
    }
}
function armaTemplateBusqueda(array_busqueda){
    let template = '', indice = 0;
    for (let i = 0; i < array_busqueda.length; i++) {
        indice = array_busqueda[i];
        producto = arrayMisproductos[indice];
        template += `<article>
        <h2 class="descripcion">${arrayMisproductos[i].descripcion}</h2>
        <img src="${arrayMisproductos[i].imagen}" class="imagen" >
                 </article>`
    }
    return template;
}
function salir_busqueda(){
    buscar.value = "";
    seccionProducto.innerHTML = armarTemplate();
    document.querySelector("#agregar").classList.remove("disable");
    salir_busk.disabled = true;
}




